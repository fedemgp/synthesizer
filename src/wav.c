/*
	Algoritmos y Programacion I - 75.02
	Curso Ing. Cardozo
	Trabajo práctico final: Sintetizador de audio
	
	Autores:
	
	Garcia, Martín Cruz
	Gomez Peter, Federico Manuel
													*/

/*wav.c*/
#include "common.h"
#include "score.h"
#include "instrument.h"
#include "samples.h"
#include "args.h"
#include "wav.h"



status_t set_new_wav(ADT_score_t **ADTscore,ADT_header_t **ADTheader,ADT_instrument_t **ADTinstrument,uint16 **data,unsigned int samplerate,int argc, char **argv)
{
	status=create_ADT_score(ADTscore);
	if(status!=OK)
		return status;

	status=create_ADT_instrument(ADTinstrument);
	if(status!=OK){
		free(ADTscore);
		return status;
	}

	if((status=ADT_header_new(ADTheader))!=OK){
		free(ADTscore);
		free(ADTinstrument);
		return status;
	}

	if((status=ADT_read_instrument_file(get_argument(argc, argv, INSTRUMENT_ARG_FLAG), *ADTinstrument))!=OK){
		free(*ADTscore);
		free(*ADTinstrument);
		free(*ADTheader);
		return status;
	}

	if((status=ADT_new_score(get_argument(argc, argv, SCORE_ARG_FLAG), *ADTscore))!=OK){
		ADT_instrument_destroy(*ADTinstrument);
		free(*ADTscore);
		free(*ADTinstrument);
		free(*ADTheader);
		return status;
	}

	if((status=malloc_data(data, (*ADTscore)->maxtime, samplerate))!=OK){
		destroy_all (*ADTscore,*ADTinstrument,*ADTheader);
		return status;
	}
	return OK;
}
void destroy_all (ADT_score_t *score,ADT_instrument_t *instrument,ADT_header_t *header)
{
		ADT_instrument_destroy(instrument);
		ADT_destroy_score(score);
		free(score);
		free(instrument);
		free(header);
}
status_t close_wav (FILE *out)
{
	if(fclose(out)==EOF)
		return ERR_CLOSE_WAV_FILE;
	return OK;
}