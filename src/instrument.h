/*
	Algoritmos y Programacion I - 75.02
	Curso Ing. Cardozo
	Trabajo práctico final: Sintetizador de audio
	
	Autores:
	
	Garcia, Martín Cruz
	Gomez Peter, Federico Manuel
													*/

/*instrument.h*/
#ifndef INSTRUMENT_H
#define INSTRUMENT_H

#include <string.h>

#define ENVELOPES_MAX_STRING 10
#define ENVELOPES_MAX_PARAMETERS 3
#define MAX_ENVELOPE_LINE_LENGHT 25

typedef struct{

	FILE *finst;
	int armonics_quantity;
	double *armonics_multipliers, *armonics_intensity;
	char *attack_envelope, *sustain_envelope, *decay_envelope;
	float *attack_parameters, *sustain_parameters, *decay_parameters;

} ADT_instrument_t;

status_t create_ADT_instrument(ADT_instrument_t **);
void ADT_instrument_destroy(ADT_instrument_t *);
status_t ADT_read_instrument_file(char *, ADT_instrument_t *);
status_t ADT_open_instrument_file(char *, ADT_instrument_t *);
status_t ADT_instrument_malloc(ADT_instrument_t *);
void ADT_instrument_armonics_load(ADT_instrument_t *);
void ADT_instrument_envelopes_load(ADT_instrument_t *);
void envelope_line_parser(FILE *, char *, float *);

#endif