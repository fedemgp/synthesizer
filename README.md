# Sintetizador de audio
El presente código escrito en Ansi C89, genera un archivo WAV de música sin compresión a partir de dos archivos de texto. Estos archivos de entrada son: 
* La partitura de la canción, en un formato determinado.
* La información del instrumento a sintetizar, que posee datos de la forma de onda que este genera (para mas información recomiendo leer el enunciado).

## Autores
* García, Martín Cruz
* Gomez Peter, Federico Manuel

## Compilación
```
	mkdir bin
	cd bin
	cmake ..
	make
```

## Ejecución
Para ejecutar el programa se debe invocar de la siguiente forma (el orden de los argumentos puede variar):
```
	synth -p <path al archivo de partitura> -s <path al archivo de instrumento> -o <path archivo salida> [-f <frecuencia de muestreo>]
```
La frecuencia de muestreo es optativa. Los valores permitidos son:
* 8000
* 9600
* 11025
* 12000
* 16000
* 22050
* 24000
* 32000
* 44100
* 48000
* 88200
* 96000

Si no se le especifica una frecuencia de muestro, la frecuencia por default será de 44100 muestras por segundo. 