/*
	Algoritmos y Programacion I - 75.02
	Curso Ing. Cardozo
	Trabajo práctico final: Sintetizador de audio
	
	Autores:
	
	Garcia, Martín Cruz
	Gomez Peter, Federico Manuel
													*/

/*data.c*/
#include "common.h"
#include "data.h"
const char *function[]={
	"CONSTANT",
	"LINEAR",
	"INVLINEAR",
	"SIN",
	"EXP",
	"INVEXP",
	"QUARTCOS",
	"QUARTSIN",
	"HALFCOS",
	"HALFSIN",
	"LOG",
	"INVLOG",
	"TRI",
	"PULSES",
};
double (*envelope[]) (double,float*)=
{
	env_constant,
	env_linear,
	env_invlinear,
	env_sin,
	env_exp,
	env_invexp,
	env_quartcos,
	env_quartsin,
	env_halfcos,
	env_halfsin,
	env_log,
	env_invlog,
	env_tri,
	env_pulses,
};
status_t malloc_data(uint16 **data,double maxtime,size_t samplerate)
{
	size_t samples;
	samples= (maxtime)*samplerate;
	*data=(uint16 *) malloc(samples*sizeof(uint16));
	if((*data)==NULL)
		return ERR_NO_MEMORY;
	set_new_data_vector(*data,samples);
	return OK;
}


void set_new_data_vector (uint16 *data,size_t samples){
	size_t i,j;
	for(i=0;i<samples;i++)
		for(j=0;j<MAXUINTSIZE;j++)
			data[i][j]=0;
}

/*obtiene la frecuencia de la nota leida de la partitura. la funcion funciona solamente si la partitura tiene 
la nota mayor en mayuscula, el sostenido y bemol como 's' y 'b' (no se acepta mayuscula en este campo).
si la octava sobrepasa la decima octava, la funcion devuelve mensaje de error(ya que no escuchamos sonido pasada la
decima octava)*/
status_t get_freq (const char *chord,double * freq)
{	
	char aux1=0;
	long int aux=0;
	if(chord[TONE]<='G'&& chord[TONE]>='A')
	{
		aux1=chord[TONE]-'A';
		*freq= nota[(chord_t) aux1];
		if(chord[SHARP]=='s'||chord[SHARP]=='b')
		{
			(*freq)*=SEMITONO;
			if(((aux=strtol(&chord[OCTAVE],NULL,DECIMALBASE))<MAXOCTAVE))
				(*freq)*=pow(2,aux- FOURTHOCTAVE);
			else return ERR_WRONG_CHORD;		
		}
		else if (((aux=strtol(&chord[OCTAVEWOSHARP],NULL,DECIMALBASE))<MAXOCTAVE))
			(*freq)*=pow(2,aux- FOURTHOCTAVE);
		else return ERR_WRONG_CHORD;
	}
	else return ERR_WRONG_CHORD;
	return OK;
}

void data_destroy(uint16 *data)
{
	free(data);
}

status_t set_samples_in_data_vector(ADT_score_t *score, ADT_instrument_t *inst, unsigned int samplerate, uint16 *data){

	size_t i;
	double j, init_t, end_t;
	double tone, mod, sample;
	int amp=AMPLITUDE;
	uint16 aux;
	envelope_t attack,sustain,decay;

	attack=get_envelope(inst->attack_envelope);
	sustain=get_envelope(inst->sustain_envelope);
	decay=get_envelope(inst->decay_envelope);
	if((status=check_envelopes(attack,sustain,decay))!=OK)
		return status;

	for(i=0; i<(score->size_chord); i++){
		
		init_t=((score->time_start)[i]);
		end_t=(init_t)+((score->duration)[i]);

		for(j=init_t; j<=(end_t+0.5); j+=(SAMPLENMB)/samplerate){
		
		tone=tone_generator(inst, (score->chord)[i], (score->time_start)[i], j);
		mod=tone_modulation((score->time_start)[i], (score->duration)[i], j, inst, attack, sustain, decay);

		sample=(int)(amp*tone*mod);
		int_to_uint16(sample, aux);

		data[(size_t)(j*samplerate)][FIRSTBYTE]+=aux[FIRSTBYTE];
		data[(size_t)(j*samplerate)][SECONDBYTE]+=aux[SECONDBYTE];
		}
	}

	return OK;
}
status_t check_envelopes (envelope_t attack,envelope_t sustain,envelope_t decay)
{
	if((status=check_attack(attack))!=OK)
		return status;
	if((status=check_sustain(sustain))!=OK)
		return status;
	if((status=check_decay(decay))!=OK)
		return status;
	return OK;
}
status_t check_attack (envelope_t env)
{
	if(env!=LINEAR && env!=EXP && env!=QUARTSIN && env!=HALFSIN &&env!=LOG && env!=TRI)
		return ERR_WRONG_ATTACK;
	return OK;
}
status_t check_sustain (envelope_t env)
{
	if(env!=CONSTANT && env!=INVLINEAR && env!=INVEXP && env!=QUARTCOS &&env!=HALFCOS && env!=INVLOG && env!=PULSES)
		return ERR_WRONG_SUSTAIN;
	return OK;	
}
status_t check_decay (envelope_t env)
{
	if(env!=INVLINEAR && env!=INVEXP && env!=QUARTCOS && env!=HALFCOS &&env!=INVLOG)
		return ERR_WRONG_DECAY;
	return OK;
}
envelope_t get_envelope(char *env)
{
	size_t i;
	for (i=0;i<MAXENVELOPES;i++)
	{
		if(!strcmp(env,function[i]))
			return i;
	}
	return i;
}

double tone_generator(ADT_instrument_t *inst, char *note, double t0, double t){

	size_t j;
	double tone=0;
	double fund_freq;

	get_freq(note, &fund_freq);
	for(j=0; j<(inst->armonics_quantity); j++){
		tone+=((inst->armonics_intensity)[j])*sin(2*PI*fund_freq*((inst->armonics_multipliers)[j])*(t-t0));
	}

	return tone;
}

double tone_modulation(double t0, double d, double t, ADT_instrument_t *inst, envelope_t attack, envelope_t sustain, envelope_t decay){

	if(t0<t && t<(t0+((inst->attack_parameters)[FIRSTPARAM]))){
		return envelope[attack](t-t0, inst->attack_parameters);
	}
	if((t0+(inst->attack_parameters[FIRSTPARAM]))<t && t<(t0+d)){
		return envelope[sustain](t-(t0+((inst->attack_parameters)[FIRSTPARAM])), inst->sustain_parameters);
	}
	if((t0+d)<t && t<(t0+((inst->decay_parameters)[FIRSTPARAM])+d)){
		return envelope[sustain](d-((inst->decay_parameters)[FIRSTPARAM]), inst->sustain_parameters)*envelope[decay](t-(t0+d), inst->decay_parameters);
	}

	return 0;
}
double env_constant(double t, float *param){

	return 1;	
}

double env_linear(double t, float *param){

	return (t/param[FIRSTPARAM]);
}

double env_invlinear(double t, float *param){

	double aux;
	aux=(1-(t/(param[FIRSTPARAM])));
	if(aux>0) 
		return aux;
	return 0;
}

double env_sin(double t, float *param){

	return 1+(param[FIRSTPARAM])*sin((param[SECONDPARAM])*t);
}

double env_exp(double t, float *param){

	return pow(EULER, ((5*(t-(param[FIRSTPARAM])))/param[FIRSTPARAM]));
}

double env_invexp(double t, float *param){

	return pow(EULER, ((-5*t)/param[FIRSTPARAM]));
}

double env_quartcos(double t, float *param){

	return cos((PI*t)/(2*param[FIRSTPARAM]));
}

double env_quartsin(double t, float *param){

	return sin((PI*t)/(2*param[FIRSTPARAM]));
}

double env_halfcos(double t, float *param){

	return ((1+cos((PI*t)/(2*param[FIRSTPARAM])))/2);
}

double env_halfsin(double t, float *param){

	return (1+sin(PI*(((t/param[FIRSTPARAM])/t)-0.5)))/2;
}

double env_log(double t, float *param){

	return log10(((9*t)/param[FIRSTPARAM])+1);
}

double env_invlog(double t, float *param){

	if(param[FIRSTPARAM]<=t)
		return 0;
	return log10(((-9*t)/param[FIRSTPARAM])+10);
}

double env_tri(double t, float *param){

	if(t>param[FIRSTPARAM])
		return ((t-param[SECONDPARAM])/(param[SECONDPARAM]-param[FIRSTPARAM]))*(param[THIRDPARAM]-1)+1;
	return (t*param[THIRDPARAM])/param[SECONDPARAM];
}

double env_pulses(double t, float *param){

	double t_prime, aux;

	t_prime=((t/param[FIRSTPARAM])-(int)(t/param[FIRSTPARAM]))*param[FIRSTPARAM];
	aux=((1-param[THIRDPARAM])/param[SECONDPARAM])*(t_prime-param[FIRSTPARAM]+param[SECONDPARAM]);
	aux=fabs(aux);
	aux+=param[THIRDPARAM];

	if(aux<1)
		return aux;
	return 1;
}
