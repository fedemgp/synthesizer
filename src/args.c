/*
	Algoritmos y Programacion I - 75.02
	Curso Ing. Cardozo
	Trabajo práctico final: Sintetizador de audio
	
	Autores:
	
	Garcia, Martín Cruz
	Gomez Peter, Federico Manuel
													*/

/*args.c*/
#include "common.h"
#include "args.h"

const double possible_sample_rates[]={
	8000,
	9600,
	11025,
	12000,
	16000,
	22050,
	24000,
	32000,
	44100,
	48000,
	88200,
	96000,
};

status_t arguments_validation(int argc, char **argv){

	if(argc<MINARGC||argc>MAXARGC){
		return ERR_WRONG_ARGUMENTS_QUANTITY;
	}
	if(flag_validation(argc, argv, INSTRUMENT_ARG_FLAG)!=OK) return ERR_WRONG_ARGUMENTS;
	if(flag_validation(argc, argv, SCORE_ARG_FLAG)!=OK) return ERR_WRONG_ARGUMENTS;
	if(flag_validation(argc, argv, AUDIO_OUTPUT_ARG_FLAG)!=OK) return ERR_WRONG_ARGUMENTS;

	return OK;
}

status_t flag_validation(int argc, char** argv, char *flag){

	size_t j;
	for(j=1; j<argc; j++){
		if(!strcmp(argv[j], flag))
			return OK;
	}
	return ERR_WRONG_ARGUMENTS;
}

char *get_argument(int argc, char **argv, char *flag){

	size_t k;
	for(k=1; k<argc; k++){
		if(!strcmp(argv[k], flag))
			return argv[k+1];
	}
	return NULL;
}


status_t get_sample_rate(int argc, char **argv, unsigned int *samplerate){

	char *sr;
	if((sr=get_argument(argc, argv, FREQUENCY_ARG_FLAG))==NULL){
		(*samplerate)=DEFAULT_SAMPLE_RATE;
		return OK;
	}

	(*samplerate)=atof(sr);
	if(validate_sample_rate(*samplerate)!=OK)
		return ERR_SAMPLE_RATE_INPUT;
	return OK;

}

status_t validate_sample_rate(double sr){

	size_t j;
	for(j=0;j<MAX_POSSIBLE_SAMPLE_RATES;j++){
		if(sr==possible_sample_rates[j])
			return OK;
	}
	return ERR_SAMPLE_RATE_INPUT;
}