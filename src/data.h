/*
	Algoritmos y Programacion I - 75.02
	Curso Ing. Cardozo
	Trabajo práctico final: Sintetizador de audio
	
	Autores:
	
	Garcia, Martín Cruz
	Gomez Peter, Federico Manuel
													*/

/*data.h*/
#ifndef DATA_H
#define DATA_H

#define PI 3.14159265359
#define MAXENVELOPES 14
#define EULER 2.71828182846
#define MAXUINTSIZE 2
#define TONE 0
#define SHARP 1
#define SEMITONO 1.059463
#define OCTAVE 2
#define DECIMALBASE 10
#define MAXOCTAVE 11
#define FOURTHOCTAVE 4
#define OCTAVEWOSHARP 1
#define AMPLITUDE 1500
#define SAMPLENMB 1.0
#define FIRSTBYTE 0
#define SECONDBYTE 1
#define FIRSTPARAM 0
#define SECONDPARAM 1
#define THIRDPARAM 2

#include "common.h"
#include "instrument.h"
#include "score.h"
#include "samples.h"

typedef enum{CONSTANT=0,LINEAR,INVLINEAR,SIN,EXP,INVEXP,QUARTCOS,QUARTSIN,HALFCOS,HALFSIN,LOG,INVLOG,TRI,PULSES} envelope_t;


extern const double nota[];

status_t malloc_data(uint16 **,double,size_t );
void set_new_data_vector (uint16 *,size_t );
status_t get_freq (const char *,double *);
void data_destroy(uint16 *);

status_t check_envelopes (envelope_t,envelope_t,envelope_t );
status_t check_attack (envelope_t);
status_t check_sustain (envelope_t );
status_t check_decay (envelope_t );
envelope_t get_envelope(char *);

 status_t set_samples_in_data_vector(ADT_score_t *, ADT_instrument_t *, unsigned int , uint16 *);
double tone_generator(ADT_instrument_t *, char *, double , double );
double tone_modulation(double , double , double , ADT_instrument_t *, envelope_t, envelope_t, envelope_t);
double env_constant(double , float *);
double env_linear(double , float *);
double env_invlinear(double , float *);
double env_sin(double , float *);
double env_exp(double , float *);
double env_invexp(double , float *);
double env_quartcos(double , float *);
double env_quartsin(double , float *);
double env_halfcos(double , float *);
double env_halfsin(double , float *);
double env_log(double , float *);
double env_invlog(double , float *);
double env_tri(double , float *);
double env_pulses(double , float *);
#endif