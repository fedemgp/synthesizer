/*
	Algoritmos y Programacion I - 75.02
	Curso Ing. Cardozo
	Trabajo práctico final: Sintetizador de audio
	
	Autores:
	
	Garcia, Martín Cruz
	Gomez Peter, Federico Manuel
													*/

/*instrument.c*/


#include "common.h"
#include "instrument.h"


status_t create_ADT_instrument(ADT_instrument_t **p)
{
	*p=(ADT_instrument_t*) malloc(sizeof (ADT_instrument_t));
	if (*p==NULL)
		return ERR_NO_MEMORY;
	return OK;
}

void ADT_instrument_destroy(ADT_instrument_t *p)
{
		free(p->decay_envelope);
		free(p->sustain_envelope);
		free(p->attack_envelope);
		free(p->armonics_intensity);
		free(p->armonics_multipliers);
		free(p->attack_parameters);
		free(p->sustain_parameters);
		free(p->decay_parameters);
}

/*Crea un puntero al archivo del instrumento y carga los datos de cantidad de armónicos y
sus respectivas amplitudes																		*/
status_t ADT_read_instrument_file(char *instrument_file_path, ADT_instrument_t *p){

	if((ADT_open_instrument_file(instrument_file_path, p))!=OK) return ERR_OPEN_INSTRUMENT_FILE;

	fscanf(p->finst, "%d", &(p->armonics_quantity));

	if((ADT_instrument_malloc(p))!=OK) return ERR_NO_MEMORY;

	ADT_instrument_armonics_load(p);
	ADT_instrument_envelopes_load(p);

	return OK;
}

status_t ADT_open_instrument_file(char *instrument_file_path, ADT_instrument_t *p){

	(p->finst)=fopen(instrument_file_path, "rt");
	if((p->finst)==NULL) return ERR_OPEN_INSTRUMENT_FILE;
	return OK;

}

status_t ADT_instrument_malloc(ADT_instrument_t *p){

	(p->armonics_multipliers)=malloc(sizeof(double)*(p->armonics_quantity));
	if((p->armonics_multipliers)==NULL) return ERR_NO_MEMORY;
	(p->armonics_intensity)=malloc(sizeof(double)*(p->armonics_quantity));
	if((p->armonics_intensity)==NULL){
		free(p->armonics_multipliers);
		return ERR_NO_MEMORY;
	}
	(p->attack_envelope)=malloc(ENVELOPES_MAX_STRING*sizeof(char));
	if((p->attack_envelope)==NULL){
		free(p->armonics_multipliers);
		free(p->armonics_intensity);
		return ERR_NO_MEMORY;
	}
	(p->sustain_envelope)=malloc(ENVELOPES_MAX_STRING*sizeof(char));
	if((p->sustain_envelope)==NULL){
		free(p->armonics_multipliers);
		free(p->armonics_intensity);
		free(p->attack_envelope);
		return ERR_NO_MEMORY;
	}
	(p->decay_envelope)=malloc(ENVELOPES_MAX_STRING*sizeof(char));
	if((p->decay_envelope)==NULL){
		free(p->armonics_intensity);
		free(p->armonics_multipliers);
		free(p->attack_envelope);
		free(p->sustain_envelope);
		return ERR_NO_MEMORY;
	}

	(p->attack_parameters)=malloc(ENVELOPES_MAX_PARAMETERS*sizeof(float));
	if((p->attack_parameters)==NULL){
		free(p->decay_envelope);
		free(p->sustain_envelope);
		free(p->attack_envelope);
		free(p->armonics_intensity);
		free(p->armonics_multipliers);
		return ERR_NO_MEMORY;
	}
	(p->sustain_parameters)=malloc(ENVELOPES_MAX_PARAMETERS*sizeof(float));
	if((p->sustain_parameters)==NULL){
		free(p->attack_parameters);
		free(p->decay_envelope);
		free(p->sustain_envelope);
		free(p->attack_envelope);
		free(p->armonics_intensity);
		free(p->armonics_multipliers);
		return ERR_NO_MEMORY;
	}
	(p->decay_parameters)=malloc(ENVELOPES_MAX_PARAMETERS*sizeof(float));
	if((p->decay_parameters)==NULL){
		free(p->attack_parameters);
		free(p->sustain_parameters);
		free(p->decay_envelope);
		free(p->sustain_envelope);
		free(p->attack_envelope);
		free(p->armonics_intensity);
		free(p->armonics_multipliers);
		return ERR_NO_MEMORY;
	}

	return OK;

}

void ADT_instrument_armonics_load(ADT_instrument_t *p){

	size_t i;

	for(i=0; i<(p->armonics_quantity); i++)
		fscanf(p->finst, "%lf%lf", &((p->armonics_multipliers)[i]), &((p->armonics_intensity)[i]));

}

void ADT_instrument_envelopes_load(ADT_instrument_t *p){

	getc(p->finst);
	getc(p->finst);
	envelope_line_parser(p->finst, p->attack_envelope, p->attack_parameters);
	envelope_line_parser(p->finst, p->sustain_envelope, p->sustain_parameters);
	envelope_line_parser(p->finst, p->decay_envelope, p->decay_parameters);
}

/*obtiene del stream del instrumento la forma de la envolvente que genera el instrumento*/
void envelope_line_parser(FILE *stream, char *envelope, float *parameters){

	char aux[MAX_ENVELOPE_LINE_LENGHT];
	char *p;
	size_t i=0;

	fgets(aux, MAX_ENVELOPE_LINE_LENGHT, stream);
	p=strtok(aux, " \n");
	strcpy(envelope, p);
	while((p=strtok(NULL, " \n"))!=NULL){
		parameters[i]=atof(p);
		i++;
	}
	while(i<ENVELOPES_MAX_PARAMETERS){
		parameters[i]=0;
		i++;
	}
}