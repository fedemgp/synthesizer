/*
	Algoritmos y Programacion I - 75.02
	Curso Ing. Cardozo
	Trabajo práctico final: Sintetizador de audio
	
	Autores:
	
	Garcia, Martín Cruz
	Gomez Peter, Federico Manuel
													*/

/*main.c*/
#include "common.h"
#include "score.h"
#include "instrument.h"
#include "args.h"
#include "samples.h"
#include "wav.h"
#include "data.h"

status_t status;

const char*MSG_ERR[]={
	"",
	"El archivo de sonido se ha cerrado incorrectamente.",
	"El archivo partitura no pudo abrirse correctamente",
	"Memoria insuficiente para realizar la operacion",
	"La partitura tiene incompatibilidad de elementos. Faltan datos.",
	"Argumento nulo.",
	"El archivo de instrumentos no pudo abrirse correctamente",
	"Argumentos invalidos",
	"El archivo de partitura presenta acordes invalidos",
	"Cantidad de argumentos invalidos",
	"Valor de frecuencia invalida",
	"Tipo incompatible de funcion ataque",
	"Tipo incompatible de funcion sostenido",
	"Tipo incompatible de funcion decaimiento",};

int main(int argc, char *argv[]){

	uint16 *data=NULL;
	ADT_instrument_t *ADTinstrument=NULL;
	ADT_score_t *ADTscore=NULL;
	ADT_header_t *ADTheader=NULL;
	unsigned int samplerate;
	size_t i;
	FILE *output;
	size_t samples;
	
	if((status=arguments_validation(argc, argv))!=OK){
		fprintf(stderr, "%s\n",MSG_ERR[status] );
		return EXIT_FAILURE;
	}

	if((status=get_sample_rate(argc,argv,&samplerate))!=OK){
		fprintf(stderr, "%s\n",MSG_ERR[status] );
		return EXIT_FAILURE;
	}

	output=fopen(get_argument(argc, argv, AUDIO_OUTPUT_ARG_FLAG), "wb");
	if(output==NULL) return EXIT_FAILURE;

	if((status=set_new_wav(&ADTscore,&ADTheader,&ADTinstrument,&data,samplerate,argc,argv))!=OK)
	{
		fprintf(stderr, "%s\n",MSG_ERR[status] );
		return EXIT_FAILURE;
	}

	ADT_header_setup(ADTheader, samplerate, ADTscore->maxtime);

	ADT_header_print(ADTheader, output);

	status=set_samples_in_data_vector(ADTscore, ADTinstrument, samplerate, data);
	if(status!=OK){
		destroy_all(ADTscore,ADTinstrument,ADTheader);
		fprintf(stderr, "%s\n",MSG_ERR[status] );
		return EXIT_FAILURE;
	}
	
	samples=((ADTscore->maxtime)*samplerate);
	
	printf("Frecuencia: %d \nTiempo de duracion de cancion: %.0f seg" 
			"\nCantidad de muestras: %li\n", samplerate, ADTscore->maxtime, samples);
	
	destroy_all(ADTscore,ADTinstrument,ADTheader);
	
	for(i=0;i<samples;i++)
		fwrite(data[i],sizeof(uint16),1,output);

	destroy(data,NULL);
	
	if((status=close_wav(output))!=OK)
	{
		fprintf(stderr, "%s\n",MSG_ERR[status] );
		return EXIT_FAILURE;
	}

	return 0;
}