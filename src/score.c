/*
	Algoritmos y Programacion I - 75.02
	Curso Ing. Cardozo
	Trabajo práctico final: Sintetizador de audio
	
	Autores:
	
	Garcia, Martín Cruz
	Gomez Peter, Federico Manuel
													*/

/*score.c*/
#include "common.h"
#include "score.h"


status_t create_ADT_score (ADT_score_t **p)
{
	*p=(ADT_score_t*) malloc(sizeof (ADT_score_t));
	if (*p==NULL)
		return ERR_NO_MEMORY;
	return OK;
}

/*Crea memoria para la partitura, */
status_t ADT_new_score(char *path, ADT_score_t * p)
{
	if((status=ADT_open_file_score(path,p))!=OK) return status;	
	if((status=ADT_new_vector(p))!=OK)
	{
		ADT_close_file_score(p);
		return status;
	}
	ADT_load_score(p);
	return OK;

}
/*lee archivo y sube a memoria dinamica los datos, los tiempos deben tener reales, de otro modo
la funcion guardará 0*/
status_t ADT_load_score (ADT_score_t *p)
{
	size_t i=0;
	int aux;

	fscanf (p->score,"%lf %s %lf",&(p->time_start[i]),(p->chord[i]),&(p->duration[i]));
	i++;
	while (!feof(p->score))
	{
		if(i==p->size_time_start)
		{
			status= ADT_realloc_score(p);
			if(status!=OK)
				{
					ADT_destroy_score(p);
					return status;
				}
			if((status=ADT_checkSize(p))!=OK)
			{
				ADT_destroy_score(p);
				return status;
			}
		}
		fscanf (p->score,"%lf %s %lf",&(p->time_start[i]),(p->chord[i]),&(p->duration[i]));
		getmaxtime(p,p->time_start[i],p->duration[i]);
		i++;
	}

	aux=(p->maxtime);
	(p->maxtime)=(int)(aux+EXTRASEC);
	/*se hace un redondeo a int mas dos segundos para salvamentar cortes inmediatos de la cancion. se le da tiempo de sobra para
	que el decaimiento de la ultima nota pueda concluir */
	
	p->usedsize=i;
	if((status=ADT_adjust_score(p))!=OK)
	{
		ADT_destroy_score(p);
		return status;
	}
	return OK;
}
void getmaxtime (ADT_score_t *p,double time_start, double duration)
{	
	duration+=time_start;
	if (p->maxtime<duration)
		p->maxtime=duration;
}
/*Ajusta mediante realloc las caedenas de vectores, dejandolas del tamaño de usedsize*/
status_t ADT_adjust_score (ADT_score_t *p)
{
	double *auxd;
	char **auxc;
	auxd= (double *) realloc(p->time_start,(p->usedsize)*sizeof(double));
	if(auxd==NULL)
	{
		ADT_destroy_score(p);
		return ERR_NO_MEMORY;
	}
	p->time_start=auxd;
	auxd=(double *) realloc(p->duration,(p->usedsize)*sizeof(double));
	if(auxd==NULL)
	{
		ADT_destroy_score(p);
		return ERR_NO_MEMORY;
	}
	p->duration=auxd;
	auxc=(char**) realloc(p->chord,(p->usedsize)*sizeof(char*));
	if(auxc==NULL)
	{
		ADT_destroy_score(p);
		return ERR_NO_MEMORY;
	}
	p->size_time_start=p->size_chord=p->size_duration=p->usedsize;
	return OK;
}
/*función que aumenta el tamaño de los vectores*/
status_t ADT_realloc_score(ADT_score_t *p)
{
	status= new_double_vector(&(p->time_start),&(p->size_time_start));
	if(status!=OK)
		return status;
	status=new_double_vector(&(p->duration),&(p->size_duration));
	if(status!=OK)
		return status;
	status=new_char_vector(&(p->chord),&(p->size_chord));
	if(status!=OK)
		return status;
	return OK;
}
status_t new_char_vector(char ***chord,size_t *size)
{
	char **aux;
	size_t i;
	aux= (char**) realloc (*chord,(INIT_CHOP+(*size)) *sizeof(char*));
	if (aux==NULL)
	{
		destroy(*chord,size);
		return ERR_NO_MEMORY;
	}
	*chord=aux;
	*size+=INIT_CHOP;
	for(i=((*size)-INIT_CHOP);i<(*size);i++)
	{
		(*chord)[i]=(char*)malloc(4*sizeof(char));
			if ((*chord)[i]==NULL)
			{
				ADT_destroy_char_vector(*chord,size);
				return ERR_NO_MEMORY;
			}
	}
	return OK;
}

status_t new_double_vector(double **t,size_t *size)
{
	double *aux;
	aux=(double *)realloc(*t,(INIT_CHOP+(*size))*sizeof(double));
	if (aux==NULL)
	{
		destroy(*t,size);
		return ERR_NO_MEMORY;
	}				
	*t=aux;
	*size+=INIT_CHOP;
	return OK;
}

status_t ADT_open_file_score(char *direction,ADT_score_t *p)
{
	(p->score)=fopen(direction,"rt");
	if (p->score==NULL)
		return ERR_OPEN_SCORE_FILE;
	return OK;
}

/*esta funcion no valida el cierre del archivo ya que nunca es modificado, es solo de lectura*/
void ADT_close_file_score(ADT_score_t *p)
{
	fclose(p->score);
}
/*crea arreglos de los tiempos y la notas, preinicializadas a NUll para que se adapte
al prototipo de new vector, que usara un realloc*/
status_t ADT_new_vector(ADT_score_t *p)
{
/*inicializa los sizes y los punteros a tiempos y notas a 0 y NULL respectivamente, para que la funcion creadora de 
arreglos funcione tambien como una actualizacion de arreglo, usando realloc. realloc funciona como malloc si se le entrega 
como argumento un puntero a NULL*/
	setup_new_vector(p);
	status=new_double_vector(&(p->time_start),&(p->size_time_start));
	if(status!=OK) return status;
	status=new_double_vector(&(p->duration),&(p->size_duration));
	if(status!=OK){
		destroy(p->time_start,&(p->size_time_start));
		return status;
	}
	status=new_char_vector(&(p->chord),&(p->size_chord));
	if(status!=OK)
	{
		destroy(p->duration,&(p->size_duration));
		destroy(p->time_start,&(p->size_time_start));
		return status ;
	}
	if((status=ADT_checkSize(p))!=OK)
	{
		ADT_destroy_score(p);
		return status;
	}
	return OK;
}
void destroy(void *d,size_t*size)
{
	free(d);
	if(size!=NULL)
		*size=0;
}
status_t ADT_checkSize (ADT_score_t *p)
{
	if((p->size_time_start)!=(p->size_duration) || (p->size_time_start)!=(p->size_chord))
		return ERR_SIZE_INCOMPATIBILITY;
	return OK;
}

/*inicializa todo a NULL y size lo establece en 0*/
void setup_new_vector(ADT_score_t *p)
{
	p->time_start=NULL;
	p->duration=NULL;
	p->chord=NULL;
	p->size_chord=0;
	p->size_time_start=0;
	p->size_duration=0;
	p->usedsize=0;
	p->maxtime=0;
}


void ADT_destroy_score (ADT_score_t *p)
{
	destroy(p->time_start,&(p->size_time_start));
	destroy(p->duration,&(p->size_duration));
	ADT_destroy_char_vector(p->chord,&(p->size_chord));
}

void ADT_destroy_char_vector(char **chord,size_t *size)
{
	size_t i;
	for(i=0;i<(*size);i++)
		free(chord[i]);
	destroy(chord,size);
}
