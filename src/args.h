/*
	Algoritmos y Programacion I - 75.02
	Curso Ing. Cardozo
	Trabajo práctico final: Sintetizador de audio
	
	Autores:
	
	Garcia, Martín Cruz
	Gomez Peter, Federico Manuel
													*/

/*args.h*/
#ifndef ARGS_H
#define ARGS_H

#define FREQUENCY_ARG_FLAG "-f"
#define INSTRUMENT_ARG_FLAG "-s"
#define SCORE_ARG_FLAG "-p"
#define AUDIO_OUTPUT_ARG_FLAG "-o"
#define DEFAULT_SAMPLE_RATE 44100
#define MAX_POSSIBLE_SAMPLE_RATES 12

#define MINARGC 7
#define MAXARGC 9

status_t arguments_validation(int, char **);
status_t flag_validation(int,char **, char *);
char *get_argument(int, char **, char *);
status_t get_sample_rate(int, char **, unsigned int *);
status_t validate_sample_rate(double);

#endif