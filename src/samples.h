/*
	Algoritmos y Programacion I - 75.02
	Curso Ing. Cardozo
	Trabajo práctico final: Sintetizador de audio
	
	Autores:
	
	Garcia, Martín Cruz
	Gomez Peter, Federico Manuel
													*/

/*SAMPLES.h*/
#ifndef SAMPLES_H

#include <math.h>
#define SAMPLES_H
#define SEMITONO 1.059463

#define CHUNKID "RIFF"
#define FORMAT "WAVE"
#define SUBCHUNK1ID "fmt "
#define SUBCHUNK2ID "data"
#define SUBCHUNK1SIZE 16
#define AUDIOFORMAT 1
#define NUMCHANNELS 1
#define BLOCKALIGN 2
#define BITSPERSAMPLE 16
#define MASK 0xFF
#define HEADERSIZE 36
#define ONEBYTE 8
#define TWOBYTES 16
#define THREEBYTES 24
#define MAXCAD 4


typedef char uint32[4];
typedef char uint16[2];

typedef enum {A,B,C,D,E,F,G} chord_t;

typedef struct{
	char ChunkId[4];
	uint32 ChunkSize;
	char Format[4];
	char Subchunk1ID[4];
	uint32 Subchunk1Size;
	uint16 AudioFormat;
	uint16 NumChannels;
	uint32 SampleRate;
	uint32 ByteRate;
	uint16 BlockAlign;
	uint16 BitsPerSample;
	char Subchunk2ID[4];
	uint32 Subchunk2Size;
} ADT_header_t;

status_t ADT_header_new (ADT_header_t **);
void destroy_header(ADT_header_t *);
void destroy(void *,size_t*);
void ADT_header_setup(ADT_header_t *,unsigned int,double);
void variable_setup(char *, const char *);
void int_to_uint32(unsigned int , uint32 );
void int_to_uint16(unsigned int , uint16 );
void ADT_header_print (ADT_header_t *,FILE *);
#endif