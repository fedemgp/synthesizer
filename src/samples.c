/*
	Algoritmos y Programacion I - 75.02
	Curso Ing. Cardozo
	Trabajo práctico final: Sintetizador de audio
	
	Autores:
	
	Garcia, Martín Cruz
	Gomez Peter, Federico Manuel
													*/

/*samples.c*/
#include "common.h"
#include "samples.h"


const double nota[]={
	440,
	493.884,
	523.252,
	587.332,
	659.259,
	698.461,
	783.997,
};


status_t ADT_header_new (ADT_header_t **h)
{
	*h=(ADT_header_t *) malloc(sizeof (ADT_header_t));
	if (*h==NULL)
		return ERR_NO_MEMORY;
	return OK;
}

void destroy_header(ADT_header_t *h)
{
		destroy(h,NULL);
}

/*imprime el header en el archivo final .wav */
void ADT_header_print(ADT_header_t *h,FILE*output)
{
	fwrite(h,sizeof(ADT_header_t),1,output);
}

void ADT_header_setup(ADT_header_t *p, unsigned int sR, double maxtime){
	size_t data;
	data= (size_t) (maxtime)*sR;
	variable_setup(p->ChunkId,CHUNKID);
	int_to_uint32(HEADERSIZE+(2*data), p->ChunkSize);
	variable_setup(p->Format,FORMAT);
	variable_setup(p->Subchunk1ID,SUBCHUNK1ID);
	int_to_uint32(SUBCHUNK1SIZE, p->Subchunk1Size);
	int_to_uint16(AUDIOFORMAT, p->AudioFormat);
	int_to_uint16(NUMCHANNELS, p->NumChannels);
	int_to_uint32(sR, p->SampleRate);
	int_to_uint32(2*sR, p->ByteRate);
	int_to_uint16(BLOCKALIGN, p->BlockAlign);
	int_to_uint16(BITSPERSAMPLE, p->BitsPerSample);
	variable_setup(p->Subchunk2ID,SUBCHUNK2ID);
	int_to_uint32(2*data, p->Subchunk2Size);
}

void int_to_uint32(unsigned int u, uint32 v){

	v[0]=(u& MASK);
	v[1]=(u>>ONEBYTE)& MASK;
	v[2]=(u>>TWOBYTES)& MASK;
	v[3]=(u>>THREEBYTES)& MASK;
}

void int_to_uint16(unsigned int u, uint16 v){

	v[0]=(u& MASK);
	v[1]=(u>>ONEBYTE)& MASK;
}

void variable_setup(char *variable, const char * cad)
{
	size_t i;
	for(i=0;i<MAXCAD;i++)
	{
		variable[i]=cad[i];
	}
}
