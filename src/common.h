/*
	Algoritmos y Programacion I - 75.02
	Curso Ing. Cardozo
	Trabajo práctico final: Sintetizador de audio
	
	Autores:
	
	Garcia, Martín Cruz
	Gomez Peter, Federico Manuel
													*/

/*common*/

#ifndef COMMON_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define COMMON_H

#define INIT_CHOP 100
typedef enum {
	OK, 
	ERR_CLOSE_WAV_FILE,
	ERR_OPEN_SCORE_FILE,
	ERR_NO_MEMORY,
	ERR_SIZE_INCOMPATIBILITY,
	ERR_NULL_PTR,
	ERR_OPEN_INSTRUMENT_FILE,
	ERR_WRONG_ARGUMENTS,
	ERR_WRONG_CHORD,
	ERR_WRONG_ARGUMENTS_QUANTITY,
	ERR_SAMPLE_RATE_INPUT,
	ERR_WRONG_ATTACK,
	ERR_WRONG_SUSTAIN,
	ERR_WRONG_DECAY

} status_t;

extern status_t status;
#endif