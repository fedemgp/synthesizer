/*
	Algoritmos y Programacion I - 75.02
	Curso Ing. Cardozo
	Trabajo práctico final: Sintetizador de audio
	
	Autores:
	
	Garcia, Martín Cruz
	Gomez Peter, Federico Manuel
													*/

/*wav.h*/

#ifndef WAV_H
#define WAV_H

status_t set_new_wav(ADT_score_t **,ADT_header_t **,ADT_instrument_t **,uint16 **,unsigned int,int,char** );
status_t malloc_data(uint16 **,double,size_t );
void destroy_all (ADT_score_t *,ADT_instrument_t *,ADT_header_t *);
status_t close_wav (FILE *);


#endif