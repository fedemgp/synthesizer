/*
	Algoritmos y Programacion I - 75.02
	Curso Ing. Cardozo
	Trabajo práctico final: Sintetizador de audio
	
	Autores:
	
	Garcia, Martín Cruz
	Gomez Peter, Federico Manuel
													*/

/*score.g*/
#ifndef score_H

#define score_H
#define EXTRASEC 2
typedef struct score{
	FILE * score;
	double *time_start;
	double *duration;
	char ** chord;
	size_t size_duration,size_time_start,size_chord,usedsize;
	double maxtime;
}ADT_score_t;

status_t create_ADT_score (ADT_score_t **);
status_t ADT_new_score(char*,ADT_score_t *);
status_t ADT_load_score (ADT_score_t *);
status_t ADT_realloc_score(ADT_score_t *);
status_t ADT_open_file_score(char *,ADT_score_t *);
status_t ADT_checkSize (ADT_score_t *);
void ADT_close_file_score(ADT_score_t *);
status_t ADT_new_vector(ADT_score_t *);
void setup_new_vector(ADT_score_t *);
status_t new_char_vector(char ***,size_t *);
status_t new_double_vector(double **,size_t *);
void destroy(void *,size_t*);
void ADT_destroy_score (ADT_score_t *);
void ADT_destroy_char_vector(char **,size_t *);
status_t ADT_adjust_score (ADT_score_t *);
void getmaxtime(ADT_score_t *, double, double);

#endif